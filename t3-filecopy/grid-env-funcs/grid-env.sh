if [ "X${GLITE_ENV_SET+X}" = "X" ]; then
    # Functions for manipulating the grid environment
    #
    # gridpath_prepend - prepend a value to a path-like environmnet variable
    #                                     - puts a ':' to the end end the beginning of the variable
    #                                     - removes the value from the string and removes duplicate '::'
    #                                     - prepend the value to the beginning of the variable
    #                                     - removes duplicate '::' and removes ':' from the beginning and the end variable
    #                                     - MANPATH has to be treated specially since, :: has a meaning -> don't get removed
    #                                     - Simple : could have a meaning so if it was there in the end or the begining we should not remove it.
    #                                     - export the variable, or echos the csh syntax
    #
    # gridpath_append - the same as prepend but it appends the value to the end of the variable
    # gridpath_delete  - delete a value from an environment variable. if the value becomes null string then it unsets the environment variable
    # gridemv_set     - sets an environment variable
    # gridemv_unset   - unsets an environment variable
    # gridenv_setind  - sets an environment variable if it is not already defined

    function gridpath_prepend() {
	myvar="$1"
	myvalue="$2"
	myfieldsep=":"
	mytmp="`eval echo \\$$myvar`" 

        if [ "x$mytmp" = "x$myvalue" ] || [ "x$mytmp" = "x$myfieldsep$myvalue" ] || [ "x$mytmp" = "x$myvalue$myfieldsep" ] ; then
            mytmp="${mytmp//$myvalue/}"
        else
            mytmp="${mytmp//$myfieldsep$myvalue$myfieldsep/$myfieldsep}" 		#remove if in the middle
            mytmp="${mytmp#$myvalue$myfieldsep}"					#remove if in the begining
            mytmp="${mytmp%$myfieldsep$myvalue}"					#remove if at the end
        fi

        if [ "x$mytmp" = "x" ]; then
	    mytmp="$myvalue"
        else
	    mytmp="$myvalue$myfieldsep$mytmp"
        fi

        mytmp="${mytmp//$myfieldsep$myfieldsep$myfieldsep/$myfieldsep$myfieldsep}"

        if [ "x$myvar" = "xMANPATH" ] ; then
            mytmp="${mytmp}::"
        fi
        if [ "x$ISCSHELL" = "xyes" ]; then
            echo "setenv $myvar \"$mytmp\"" 
        fi
	eval export ${myvar}=\""$mytmp"\"
    } 

    function gridpath_append() {
	myvar="$1"
	myvalue="$2"
	myfieldsep=":"
        mytmp="`eval echo \\$$myvar`"

        if [ "x$mytmp" = "x$myvalue" ] || [ "x$mytmp" = "x$myfieldsep$myvalue" ] || [ "x$mytmp" = "x$myvalue$myfieldsep" ] ; then
            mytmp="${mytmp//$myvalue/}"
        else
            mytmp="${mytmp//$myfieldsep$myvalue$myfieldsep/$myfieldsep}" 		#remove if in the middle
            mytmp="${mytmp#$myvalue$myfieldsep}"					#remove if in the begining
            mytmp="${mytmp%$myfieldsep$myvalue}"					#remove if at the end
        fi

	if [ "x$mytmp" = "x" ]; then 
            mytmp="$myvalue"
        else
   	    mytmp="$mytmp$myfieldsep$myvalue"
        fi

        mytmp="${mytmp//$myfieldsep$myfieldsep$myfieldsep/$myfieldsep$myfieldsep}"

        if [ "x$myvar" = "xMANPATH" ] ; then
            mytmp="${mytmp}::"
        fi
        if [ "x$ISCSHELL" = "xyes" ]; then
            echo "setenv $myvar \"$mytmp\"" 
        fi
	eval export ${myvar}=\""$mytmp"\"
    } 

    function gridenv_set() {
        myvar="$1"
        myvalue="$2"
        myfieldsep=":"

        if [ "x$ISCSHELL" = "xyes" ]; then
            echo "setenv $myvar \"$myvalue\"" 
        fi
        eval export ${myvar}="\"${myvalue}\""
    }

    function gridenv_setind() {
        myvar="$1"
        myvalue="$2"
        if [ "x$ISCSHELL" = "xyes" ]; then
	    echo 'if ( ${?'$myvar'} == 0 ) setenv '$myvar \'"$myvalue"\'
        fi
        eval "export $myvar=\${$myvar:-\$myvalue}"
    }

    function gridenv_unset() {
        myvar="$1"
	eval unset \""$myvar"\"
    }

    function gridpath_delete() {
        myvar="${!1}"
	myvalue="$2"
	declare myvar=$(echo $myvar | sed -e "s;\(^$myvalue:\|:$myvalue$\|:$myvalue\(:\)\|^$myvalue$\);\2;g")
        
        if [ "x$ISCSHELL" = "xyes" ]; then
            echo "setenv $1 \"$myvar\"" 
        fi
        eval export $1=\""$myvar"\"
    }
    #####################################################################
    if [ "x${GLITE_UI_ARCH:-$1}" = "x32BIT" ]; then arch_dir=lib; else arch_dir=lib64; fi
    
    gridpath_prepend     "PATH" "/bin"
    gridpath_prepend     "MANPATH" "/opt/glite/share/man"
    gridenv_setind      "X509_USER_PROXY" "$HOME/.x509up_u$(id -u)"
    gridenv_set         "X509_CERT_DIR" "/etc/grid-security/certificates"
    gridenv_set         "X509_VOMS_DIR" "/etc/grid-security/vomsdir"
    gridenv_set         "VOMS_USERCONF" "/etc/vomses"
    gridenv_set         "VO_CMS_SW_DIR" "/cvmfs/cms.cern.ch"
    gridenv_set         "VO_CMS_DEFAULT_SE" "t3se01.psi.ch"
    gridenv_set         "SRM_PATH" "/usr/share/srm"
    gridenv_set         "MYPROXY_SERVER" "myproxy.cern.ch"
    gridenv_set         "LCG_LOCATION" "/usr"
    gridenv_set         "LCG_GFAL_INFOSYS" "lcg-bdii.cern.ch:2170"
    gridenv_set         "BDII_LIST" "lcg-bdii.cern.ch:2170"
    gridenv_set         "GT_PROXY_MODE" "old"
    gridenv_set         "GRID_ENV_LOCATION" "/etc/profile.d"
    gridenv_set         "GLOBUS_TCP_PORT_RANGE" "20000,25000"
    gridenv_set         "GLITE_SD_SERVICES_XML" "/usr/etc/services.xml"
    gridenv_set         "GLITE_SD_PLUGIN" "file,bdii"
    gridenv_set         "GLITE_LOCATION_VAR" "/var"
    gridenv_set         "GLITE_LOCATION" "/usr"
    gridenv_set         "DPNS_HOST" "my-dpm.psi.ch"
    gridenv_set         "DPM_HOST" "my-dpm.psi.ch"
    #####################################################################
    # Clean up the functions
    unset   myvar
    unset   myvalue
    unset   myfieldsep
    unset   mytmp 
    unset -f gridpath_prepend 
    unset -f gridpath_append
    unset -f gridpath_delete
    unset -f gridenv_set
    unset -f gridenv_setind
fi

