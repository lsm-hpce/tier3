#!/usr/bin/python

# Puppet Managed File
#
# 12/12/2011 Original version by martinelli
# 17.04.2020 fast fix to also produce a gridmap file, which we need for webdav and REST
#            also, cleaned up a lot of the string handling. More needs to be done.

"""
THE SCRIPT WAS CREATED TO BUILD 2 dCache gPlazma FILEs, USUALLY TO BE STORED INTO: 
/etc/grid-security/grid-vorolemap
/etc/grid-security/storage-authzdb        
ACCORDING TO THE INFO FOUND INSIDE t3ldap01 

FOR REFERENCE: 
http://webcms.ba.infn.it/cms-software/index.html/index.php/Main/CmsUserMapping
https://wiki.chipp.ch/twiki/bin/view/CmsTier3/NodeTypeStorageElement#Automatic_creation_of_the_files
"""

# grid-vorolemap_template example
"""
# This file maps VOMS information to local user accounts

# DN   VOMS    local_account

$DNs_VOMSROLE_LINUXUSERs


#
"*" "/ops" opsuser
"*" "/ops/Role=lcgadmin" opsuser
#
"*" "/dteam" dteamuser
#
"*" "/cms" cmsuser
"*" "/cms/Role=production" cmsuser

"""


# storage-authzdb_template example
"""
version 2.1
authorize cmsuser   read-only  501 500 / / /
authorize opsuser   read-write 800 800 / / /
authorize dteamuser read-write 810 810 / / /

$AUTHORIZE_USERs_PERMs_UID_GID

"""


import ldap
import sys
import os
import stat
import getpass
import random,string
from optparse import OptionParser
import socket
from string import Template

groups_to_include = ("cms", "dcache_adm")

usage = "usage: %prog options "
parser = OptionParser(usage)
parser.add_option("-v", "--verbose",
                  action="store_true",
                  dest="verbose",
                  help="Produce verbose information during the execution",
                  default="")
parser.add_option("-H", "--host-ssl",
                  dest="ldaphost",
                  help="An SSL LDAP server like 't3ldap01.psi.ch' ( domain psi.ch matters for SSL )"
                  , default="t3ldap01.psi.ch")
parser.add_option("-D", "--output-directory",
                  dest="output_dir",
                  help="The directory where to store the grid-vorolemap and storage-authzdb files produced, default is the current directory.",
                  default="./")

(options, args) = parser.parse_args()

NO_OPTIONS_PROVIDED={'ldaphost': '', 'verbose': ''}

if options  == NO_OPTIONS_PROVIDED:
      parser.print_help()
      sys.exit(0)

if options.ldaphost == "":
      print "ERROR: -H/--host-ssl is a required option"
      sys.exit(1)
else: 
      try: 
         socket.gethostbyname(options.ldaphost)
      except socket.gaierror, e:
         print "The hostname %s is not mapped inside the DNS, maybe a typo?" % options.ldaphost
         sys.exit(1)


# The output directory provided like an option must to be a real directory 
assert stat.S_ISDIR( os.stat(options.output_dir)[stat.ST_MODE] )

# without a reachable SSL LDAP server we can't do anything
try:
      ldaptree = ldap.initialize("ldaps://"+options.ldaphost)
except ldap.LDAPError, e:
      print e.message['info']
      if type(e.message) == dict and e.message.has_key('desc'):
         print e.message['desc']
      else:         print e
      sys.exit(1)


with open('gridmap_template','r') as f:
      gridmap_template_str = f.read()
template_gridmap = Template(gridmap_template_str)

with open('grid-vorolemap_template','r') as f:
      vorolemap_template_str = f.read()
template_vorolemap = Template(vorolemap_template_str)

with open('storage-authzdb_template', 'r') as f:
      storage_authzdb_template_str = f.read()
template_authzdb = Template(storage_authzdb_template_str)

groupinfo = {}
for grp in groups_to_include:
      groupinfo[grp] = {}
      ldap_groupusers = ldaptree.search_s(base="ou=groups,dc=cmst3,dc=psi,dc=ch",
                                      scope=ldap.SCOPE_SUBTREE,
                                      filterstr="(cn=%s)" % grp,
                                      attrlist=['gidNumber','memberUid'],
                                      attrsonly=0 )
      #print(ldap_groupusers[0][1]['memberUid'])
      groupinfo[grp]['memberUid'] = ldap_groupusers[0][1]['memberUid']
      groupinfo[grp]['gidNumber'] = ldap_groupusers[0][1]['gidNumber'][0]
#print(groupinfo)

ldap_userlist  = ldaptree.search_s( base="ou=People,dc=cmst3,dc=psi,dc=ch",
                                      scope=ldap.SCOPE_SUBTREE,
                                      filterstr="(subjectDN~=/)",
                                      attrlist=['uid','subjectDN','uidNumber',
                                                'gidNumber','loginShell'],
                                      attrsonly=0 )

tmp_gridmap = ''
tmp_authzdb = ''
tmp_vorolemap = ''
for user in ldap_userlist:
    uid = user[1]['uid'][0]
    uidnumber = user[1]['uidNumber'][0]
    gidnumber = user[1]['gidNumber'][0]
    
    if user[1]['loginShell'] == '/sbin/nologin' : pass # we skip ghost users 
    assert ''.join(user[1]['subjectDN']) != ''

    tmp_gridmap += '%-95s %s\n' % ('"' + user[1]['subjectDN'][0] + '"',
                                     user[1]['uid'][0])

    tmp_vorolemap += '%-95s  "/cms"   %s\n' % ('"' + user[1]['subjectDN'][0]
                                               + '"',
                                               user[1]['uid'][0])

    secnd_grps = []
    for grp in groups_to_include:
          if uid in groupinfo[grp]['memberUid']:
                if gidnumber != groupinfo[grp]['gidNumber']:
                      secnd_grps.append(groupinfo[grp]['gidNumber'])

    gids = ','.join([gidnumber] + secnd_grps)

    tmp_authzdb += 'authorize %-16s read-write %-8s %s  / / /\n' % (uid,
                                                                 uidnumber,
                                                                 gids)

with open(options.output_dir + '/grid-vorolemap', 'w') as f:
      f.write(template_vorolemap.substitute(DNs_VOMSROLE_LINUXUSERs=tmp_vorolemap))
if options.verbose != "":
      print 'Created file: ' + options.output_dir + '/grid-vorolemap'

with open(options.output_dir + '/storage-authzdb', 'w') as f:
      f.write(template_authzdb.substitute(AUTHORIZE_USERs_PERMs_UID_GID=tmp_authzdb))
if options.verbose != "" :
      print 'Created file: ' + options.output_dir + '/storage-authzdb'

with open(options.output_dir + '/grid-mapfile', 'w') as f:
      f.write(template_gridmap.substitute(GRIDMAP_ENTRIES=tmp_gridmap))

sys.exit(0)
