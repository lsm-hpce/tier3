#!/bin/bash
curl -o /tmp/info.xml http://localhost:2288/info 
/usr/sbin/dcache-storage-descriptor file:/tmp/info.xml
scp -i /root/.ssh/id_rsa.monitoring /var/spool/dcache/storage-descriptor.json t3ganglia:/usr/share/ganglia/cric_srr/. 
rm -rf   /tmp/info.xml /var/spool/dcache/storage-descriptor.json
