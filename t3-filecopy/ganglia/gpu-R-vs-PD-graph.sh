#!/bin/bash
#
partition=gpu
if [ $# == 1 ]
then
  partition=$1
fi

#
Total=/var/lib/ganglia/rrds/servers/t3slurm.psi.ch/${partition}_Total.rrd
R=/var/lib/ganglia/rrds/servers/t3slurm.psi.ch/${partition}_R.rrd
PD=/var/lib/ganglia/rrds/servers/t3slurm.psi.ch/${partition}_PD.rrd

if [[ ! -s ${PD} && ! -s ${R} && ! -s ${Total} ]]
then
  exit 1
fi

for interval in day week month year
do
/usr/bin/rrdtool graph ${partition}-gpu-R-vs-PD-${interval}.png \
--end now --start end-1${interval} \
-w 366 -h 123 -a PNG \
--slope-mode \
--title  "Slurm ${partition} partition usage in the last ${interval}" \
--watermark "`date`" \
--vertical-label "Number of GPUs" \
--right-axis-label "Number of GPUs" \
--lower-limit 0 \
--right-axis 1:0 \
DEF:R=${R}:sum:AVERAGE \
DEF:PD=${PD}:sum:AVERAGE \
CDEF:PDN=PD,-1,* \
DEF:Total=${Total}:sum:AVERAGE \
LINE3:Total#4444ff:"Total GPU Number of ${partition} Partition" \
AREA:R#00ff00:"Running Jobs GPU Number" \
AREA:PDN#FF0000:"Waiting Jobs GPU Number" \

mv ${partition}-gpu-R-vs-PD-${interval}.png /usr/share/ganglia/slurm_monitoring/.

done

