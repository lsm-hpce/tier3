#!/bin/bash
#
free=/var/lib/ganglia/rrds/servers/t3se01.psi.ch/storage_volume_free.rrd
total=/var/lib/ganglia/rrds/servers/t3se01.psi.ch/storage_volume_total.rrd

if [[ ! -s ${free} && ! -s ${total} ]]
then
  exit 1
fi

for interval in day week month year
do
/usr/bin/rrdtool graph se-${interval}.png \
--end now --start end-1${interval} \
-w 366 -h 123 -a PNG \
--slope-mode \
--title  "SE Volumes last ${interval}" \
--watermark "`date`" \
--vertical-label "dcache space,TB" \
--right-axis-label "dcache space,TB" \
--lower-limit 0 \
--right-axis 1:0 \
DEF:free=${free}:sum:AVERAGE \
DEF:total=${total}:sum:AVERAGE \
AREA:total#0000ff:"total" \
AREA:free#00ff00:"free" \

mv se-${interval}.png /usr/share/ganglia/se_monitoring/.
done
