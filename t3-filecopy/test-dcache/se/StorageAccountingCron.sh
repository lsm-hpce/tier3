#!/bin/bash

# crontab for EMI storage accounting: https://wiki.egi.eu/wiki/APEL/SSM 

LOG=/var/log/StorageAccounting.log

date            >> $LOG
/usr/bin/dcache-star
date            >> $LOG
#sleep 30m
/usr/bin/ssmsend  >>$LOG 2>&1


